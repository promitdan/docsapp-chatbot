function insertChat(event) {
    var input = event.target;
    console.log(input.parentNode);
    var temp = input;
    var msg = document.getElementById(input.parentNode.children[0].id).value;
    var timeStamp = returnTimeStamp();
    var name = input.parentNode.parentNode.id;
    chatData.push({
        name,
        timeStamp,
        msg,
    });
    updateChats({name, timeStamp, msg});
    sendGetRequest(msg, chatBotResponse);
    document.getElementById(input.parentNode.children[0].id).value = "";
}
function updateChats(chatDetails) {
    var chats = document.getElementsByClassName('chatBox');
    if(chatDetails.name === chats[0].id) {
        chats[0].children[1].appendChild(createChatRow(chatDetails, true));
    } else {
        chats[0].children[1].appendChild(createChatRow(chatDetails, false));
    }
    chats[0].children[1].scrollTop = chats[0].children[1].scrollHeight;
}
var id = {value : 63906}

function sendGetRequest(message, callback) {
    $.ajax(
        {
            url: 'https://www.personalityforge.com/api/chat/',
            type: 'GET',
            data: {
                apiKey:'6nt5d1nJHkqbkphe', 
                chatBotID: id.value , 
                externalID: 'Promit', 
                message: message,
            },
            success: callback
        }
    )
}

function chatBotResponse(response, status, xhr) {
    console.log(response);
    console.log(status);
    var response = JSON.parse(response);
    if(status === 'success') {
         var currentChat = {};
         currentChat.name = response.message.chatBotName;
         currentChat.msg = response.message.message;
         currentChat.timeStamp = returnTimeStamp();
         chatData.push(currentChat);
         console.log(chatData);
         updateChats(currentChat);
    }

}

function returnTimeStamp() {
    var tempDate = new Date();
    var timeStamp = tempDate.getHours() + ":" + tempDate.getMinutes(); 
    return timeStamp;
}

